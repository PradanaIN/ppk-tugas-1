<?php

namespace App\Controllers;

class Pages extends BaseController
{
    public function index()
    {
        // data for web title
        $data = [
            'title' => 'Home | Belajar CodeIgniter'
        ];

        echo view('pages/home', $data);
    }

    public function cnn()
    {


        $xml_cnn = file_get_contents('https://www.cnnindonesia.com/ekonomi/rss');
        $json_cnn = simplexml_load_string($xml_cnn);

        $data = [
            'title' => 'Berita Online CNN',
            'xml_cnn' => $json_cnn
        ];

        // bisa return karena cuma 1
        return view('pages/cnn', $data);
    }

    public function tempo()
    {

        $xml_tempo = file_get_contents('https://rss.tempo.co/bisnis');
        $json_tempo = simplexml_load_string($xml_tempo);

        $data = [
            'title' => 'Berita Online Tempo',
            'xml_tempo' => $json_tempo
        ];

        // bisa return karena cuma 1
        return view('pages/tempo', $data);
    }

    public function republika()
    {

        $xml_republika = file_get_contents('https://www.republika.co.id/rss');
        $json_republika = simplexml_load_string($xml_republika);

        $data = [
            'title' => 'Berita Online Republika',
            'xml_republika' => $json_republika
        ];

        // bisa return karena cuma 1
        return view('pages/republika', $data);
    }

    public function json()
    {

        $json_bmkg = file_get_contents('https://data.bmkg.go.id/DataMKG/TEWS/gempadirasakan.json');
        $json_portal = json_decode($json_bmkg, true);

        $data = [
            'title' => 'Data Gempa Bumi BMKG',
            'json_bmkg' => $json_portal
        ];

        // bisa return karena cuma 1
        return view('pages/json', $data);
    }

    public function about()
    {
        $data = [
            'title' => 'About Me!'
        ];

        // bisa return karena cuma 1
        return view('pages/about', $data);
    }
}
